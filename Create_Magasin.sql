drop database if exists Magasin;
create database Magasin;
USE Magasin;


CREATE TABLE Produit(
	id integer NOT NULL AUTO_INCREMENT,
	Nom varchar(50),
	Categorie varchar(50),
	constraint pk_idProduit primary key(id)
);

CREATE TABLE Prix(
	id integer NOT NULL AUTO_INCREMENT,
	Valeur float,
	Date_Debut Date,
	Date_Fin Date,
	idProduit integer,
	constraint pk_idPrix primary key(id)
);

CREATE TABLE Stoc_mv(
	id integer NOT NULL AUTO_INCREMENT,
	Date Date,
	Qt_mv integer,
	idProduit integer,
	constraint pk_idCategorie primary key(id)
);

CREATE TABLE Produit_Vente(
	id integer NOT NULL AUTO_INCREMENT,
	Quantite integer,
	idProduit integer,
	idVentes integer,
	constraint pk_idProduitVente primary key(id)
);

CREATE TABLE Ventes(
	id integer NOT NULL AUTO_INCREMENT,
	Date Date,
	Prix float,
	constraint pk_idVentes primary key(id)
);