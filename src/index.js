var express = require('express');
var app = express();	//utilisation d'express
let selectProduit = require('./BDD/selectAllProduitParNom.js');
let insertProduit = require('./BDD/insertProduit.js');
let insertProduitVente = require('./BDD/insertProduitVente.js');
let insertVentes = require('./BDD/insertVentes.js');
let insertPrix = require('./BDD/insertPrix.js');
let insertStock = require('./BDD/insertStock.js');
let connexionBase = require('./BDD/connexionBase.js');
let convertDate = require('./fonctions/convertDate.js');
let selectAllProduit = require('./BDD/selectAllProduit.js');
let selectAllVentes = require('./BDD/selectAllVentes.js');
let selectAllProduitParId = require('./BDD/selectAllProduitParId.js');
let selectAllPrixParId = require('./BDD/selectAllPrixParId.js');
let selectAllStockParId = require('./BDD/selectAllStockParId.js');
let selectAllProduitVenteParId = require('./BDD/selectAllProduitVenteParId.js');
let selectAllVentesParId = require('./BDD/selectAllVentesParId.js');
let modifStock = require('./BDD/modifStock.js');
let deleteProduitParId = require('./BDD/deleteProduitParId.js');
let deletePrixParId = require('./BDD/deletePrixParId.js');
let deleteStockParId = require('./BDD/deleteStockParId.js');
let deleteProduitVenteParId = require('./BDD/deleteProduitVenteParId.js');
let deleteVentesParId = require('./BDD/deleteVentesParId.js');

//il faut ses 3 ligne pour avoir les réception de requête post
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

var moment = require('moment');
moment().format();

var pool = connexionBase.connexionBase(); //poolde connexion de mysql

app.get('/', function(req, res) {

    res.render('accueil.ejs');

});

app.get('/insertProduit', function(req, res) {

    res.render('insertionProduit.ejs');

});

app.post('/insertProduit/valid', function(req, res) {

	if(!req.body.Produit || !req.body.Categorie || !req.body.Prix || !req.body.DateDebut || !req.body.DateFin || !req.body.Quantite)
	{
		res.render('validationInsertionProduit.ejs',{message : "Il manque des champs à remplir ! revenez en arrière"});
	}
	else
	{
		var date = new Date();	//date d'aujourd'hui
		insertProduit.insertProduit(pool,req.body.Produit,req.body.Categorie,function(id,err)
		{
			if(err) throw err;
			insertPrix.insertPrix(pool,req.body.Prix,req.body.DateDebut,req.body.DateFin,id);
			insertStock.insertStock(pool,convertDate.convertDate(date),req.body.Quantite,id);
		});
		res.render('validationInsertionProduit.ejs',{message : "Tout est enregistré dans la base de données !"});
	}

});

app.get('/insertVentes', function(req, res) {

	selectAllProduit.selectAllProduit(pool,function(result,err){
		if(err) throw err;
		res.render('insertionVentes.ejs',{result : result});
	});
});

app.post('/insertVentes/valid', function(req, res) {

	if(!req.body.Produit || !req.body.Quantite)
	{
		res.render('validationInsertionProduit.ejs',{message : "Il manque des champs à remplir ! revenez en arrière"});
	}
	else
	{
		var date = new Date();	//date d'aujourd'hui
		var prix = 0;
		var stock = 0;

		//modification du stock pour le produit (reduction du stock lors d'une vente)
		selectAllStockParId.selectAllStockParId(pool,req.body.Produit,function(resultStock,err){
			if(err) throw err;
			stock = resultStock[0].Qt_mv - req.body.Quantite;
			//function modifStock(pool,date,quantite,id)
			modifStock.modifStock(pool,date,stock,req.body.Produit);
		});

		//insertion de la vente
		selectAllPrixParId.selectAllPrixParId(pool,req.body.Produit, function(resultPrix,err){
			prix = resultPrix[0].Valeur * req.body.Quantite;
			insertVentes.insertVentes(pool,convertDate.convertDate(date),prix,function(id,err)
			{
				if(err) throw err;
				insertProduitVente.insertProduitVente(pool,req.body.Quantite,req.body.Produit,id);
			});
			res.render('validationInsertionVentes.ejs',{message : "Tout est enregistré dans la base de données !"});
		});
	}

});

app.get('/affichageProduits',function(req,res){
	selectAllProduit.selectAllProduit(pool,function(result,err){
		if(err) throw err;
		res.render('affichageProduits.ejs',{result : result});
	});
});

app.get('/affichageVentes',function(req,res){
	selectAllVentes.selectAllVentes(pool,function(result,err){
		if(err) throw err;
		for (var i = 0; i < result.length; i++) {
			result[i].Date = moment(result[i].Date).format("MMMM Do YYYY");
		}
		res.render('affichageVentes.ejs',{result : result});
	});
});

app.get('/modifStock/:id',function(req,res){
	selectAllProduitParId.selectAllProduitParId(pool,req.params.id,function(result,err)
	{
		res.render('modifStock.ejs',{id : req.params.id, result : result});
	});
});

app.post('/modifStock/valid/:id',function(req,res){
	if(!req.body.Stock)
	{
		res.render('validationModifStock.ejs',{message : "Le champs n'est pas rempli"});
	}
	else
	{
		var date = new Date();
		modifStock.modifStock(pool,convertDate.convertDate(date),req.body.Stock,req.params.id);
		res.render('validationModifStock.ejs',{message : "La modification s'est bien effectué"});

	}
});

app.get('/detail/:id',function(req,res){
	selectAllProduitParId.selectAllProduitParId(pool,req.params.id,function(resultProduit,err1){
		if(err1) throw err1;
		selectAllPrixParId.selectAllPrixParId(pool,req.params.id,function(resultPrix,err2){
			if(err2) throw err2;
			selectAllStockParId.selectAllStockParId(pool,req.params.id,function(resultStock,err3){
				if(err3) throw err3;
				resultPrix[0].Date_Debut = moment(resultPrix[0].Date_Debut).format("MMMM Do YYYY");
				resultPrix[0].Date_Fin = moment(resultPrix[0].Date_Fin).format("MMMM Do YYYY");
				resultStock[0].Date = moment(resultStock[0].Date).format("MMMM Do YYYY");
				res.render('detailProduit.ejs',{resultProduit : resultProduit, resultPrix : resultPrix, resultStock : resultStock});
			});
		});
	});
});

app.get('/detailventes/:id',function(req,res){
	selectAllVentesParId.selectAllVentesParId(pool,req.params.id,function(resultVentes,err1){
		if(err1) throw err1;
		selectAllProduitVenteParId.selectAllProduitVenteParId(pool,req.params.id,function(resultProduitVente,err2){
			if(err2) throw err2;
			selectAllProduitParId.selectAllProduitParId(pool,resultProduitVente[0].idProduit,function(resultProduit,err3){
				if(err3) throw err3;
				resultVentes[0].Date = moment(resultVentes[0].Date).format("MMMM Do YYYY");
				res.render('detailVentes.ejs',{resultVentes : resultVentes, resultProduitVente : resultProduitVente, resultProduit : resultProduit});
			});
		});
	});
});

app.get('/delete/:id',function(req,res){
	deleteProduitParId.deleteProduitParId(pool,req.params.id);
	deletePrixParId.deletePrixParId(pool,req.params.id);
	deleteStockParId.deleteStockParId(pool,req.params.id);
	res.render('validationSuppression.ejs');
});

app.get('/deleteventes/:id',function(req,res){

	var date = new Date();	//date d'aujourd'hui
	var stock = 0;

		//modification du stock pour le produit (ajout du stock lors d'une supression de vente)
		selectAllProduitVenteParId.selectAllProduitVenteParId(pool,req.params.id,function(resultProduitVente,err1){
			if(err1) throw err1;
			selectAllStockParId.selectAllStockParId(pool,resultProduitVente[0].idProduit,function(resultStock,err2){
				if(err2) throw err2;
				stock = resultStock[0].Qt_mv + resultProduitVente[0].Quantite;
				modifStock.modifStock(pool,date,stock,resultProduitVente[0].idProduit);
			});
		});
	//Supression des lignes
	deleteProduitVenteParId.deleteProduitVenteParId(pool,req.params.id);
	deleteVentesParId.deleteVentesParId(pool,req.params.id);
	res.render('validationSuppressionVentes.ejs');
});

app.use(function(req, res, next){

    res.setHeader('Content-Type', 'text/plain');

    res.status(404).send('Page introuvable !');

});

app.listen(8080);