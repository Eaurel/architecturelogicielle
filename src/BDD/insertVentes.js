function insertVentes(pool,date,prix,callback)
{
	pool.getConnection(function(err,con){
		if(err) throw err;
		var sql = "INSERT INTO Ventes(Date,Prix) VALUES(?,?)";	//INSERT INTO
		var champs = [date,prix];	//valeurs à passer
		con.query(sql,champs, function(err,result){
			if(err) throw err;
			con.release();
			return callback(result.insertId);
		});
	});
}

module.exports.insertVentes = insertVentes;