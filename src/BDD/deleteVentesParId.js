function deleteVentesParId(pool,id)
{
	pool.getConnection(function(err,con){
		if(err) throw err;
		var sql = "DELETE FROM Ventes WHERE id = ?";
		con.query(sql,id, function(err,result){
			if(err) throw err;
			con.release();
		});
	});
}

module.exports.deleteVentesParId = deleteVentesParId;