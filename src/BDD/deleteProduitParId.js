function deleteProduitParId(pool,id)
{
	pool.getConnection(function(err,con){
		if(err) throw err;
		var sql = "DELETE FROM Produit WHERE id = ?";	//INSERT INTO
		con.query(sql,id, function(err,result){
			if(err) throw err;
			con.release();
		});
	});
}

module.exports.deleteProduitParId = deleteProduitParId;