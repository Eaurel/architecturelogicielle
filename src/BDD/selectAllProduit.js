function selectAllProduit(pool,callback)
{
	pool.getConnection(function(err,con){
		if(err) throw err;
		var sql = "SELECT * FROM Produit";
		con.query(sql,function(err,result){
			if(err) throw err;
			con.release();
			return callback(result);
		});
	});
}

module.exports.selectAllProduit = selectAllProduit;