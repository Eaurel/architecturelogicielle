function selectAllProduitParNom(pool,nom,callback)
{
	pool.getConnection(function(err,con)
	{
		if(err) throw err;
		var sql = 'SELECT * FROM Produit WHERE nom = ?';
		con.query(sql,nom,function(req,result){
			if (err) throw err;
			con.release();
			return callback(result);
		});
	});
}

module.exports.selectAllProduitParNom = selectAllProduitParNom;