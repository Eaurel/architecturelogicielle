function deletePrixParId(pool,id)
{
	pool.getConnection(function(err,con){
		if(err) throw err;
		var sql = "DELETE FROM Prix WHERE idProduit = ?";	//INSERT INTO
		con.query(sql,id, function(err,result){
			if(err) throw err;
			con.release();
		});
	});
}

module.exports.deletePrixParId = deletePrixParId;