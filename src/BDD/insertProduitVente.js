function insertProduitVente(pool,quantite,idProduit,idVentes)
{
	pool.getConnection(function(err,con){
		if(err) throw err;
		var sql = "INSERT INTO Produit_Vente(quantite,idProduit,idVentes) VALUES(?,?,?)";	//INSERT INTO
		var champs = [quantite,idProduit,idVentes];	//valeurs à passer
		con.query(sql,champs, function(err,result){
			if(err) throw err;
			con.release();
			console.log("ligne inseree " + result.insertId);
		});
	});
}

module.exports.insertProduitVente = insertProduitVente;