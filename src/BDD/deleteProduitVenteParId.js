function deleteProduitVenteParId(pool,id)
{
	pool.getConnection(function(err,con){
		if(err) throw err;
		var sql = "DELETE FROM Produit_Vente WHERE idVentes = ?";	//INSERT INTO
		con.query(sql,id, function(err,result){
			if(err) throw err;
			con.release();
		});
	});
}

module.exports.deleteProduitVenteParId = deleteProduitVenteParId;