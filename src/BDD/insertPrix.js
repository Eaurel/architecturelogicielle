function insertPrix(pool,valeur,dateDebut,dateFin,id)
{
	pool.getConnection(function(err,con){
		if(err) throw err;
		var sql = "INSERT INTO Prix(Valeur,Date_Debut,Date_Fin,idProduit) VALUES(?,?,?,?)";	//INSERT INTO
		var champs = [valeur,dateDebut,dateFin,id];	//valeurs à passer
		con.query(sql,champs, function(err,result){
			if(err) throw err;
			con.release();
			console.log("ligne inseree " + result.insertId);
		});
	});
}

module.exports.insertPrix = insertPrix;