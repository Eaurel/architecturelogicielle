function selectAllPrixParId(pool,id,callback)
{
	pool.getConnection(function(err,con)
	{
		if(err) throw err;
		var sql = 'SELECT * FROM Prix WHERE idProduit = ?';
		con.query(sql,id,function(req,result){
			if (err) throw err;
			con.release();
			return callback(result);
		});
	});
}

module.exports.selectAllPrixParId = selectAllPrixParId;