/*
	fonction qui insère dans la table Produit
	passage en paramètre le nom et le prénom
	Connexion à la base pour rajouter les données
	renvoi le numéro id où la ligne est inséré (on sait jamais)
*/
function insertProduit(pool,nom,categorie,callback)
{
	pool.getConnection(function(err,con)	//connexion à la base
	{
		if(err) throw err;
		var sql = "INSERT INTO Produit(Nom,Categorie) VALUES(?,?)";	//INSERT INTO
		var champs = [nom,categorie];	//valeurs à passer
		con.query(sql,champs, function(err,result){
			if(err) throw err;
			con.release();
			return callback(result.insertId);
		});
	});
}

module.exports.insertProduit = insertProduit;